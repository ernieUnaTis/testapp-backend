from app import db
from sqlalchemy_utils import ChoiceType
from uuid import uuid4
import datetime, enum

# method to generate uuid
def generate_uuid():
    return str(uuid4())


"""
=============
model Evaluation
=============
"""

class Evaluation(db.Model):
  __tablename__ = "evaluations"
  uuid = db.Column(db.String(255), nullable=False, unique=True, default=generate_uuid, primary_key=True)
  sentence = db.Column(db.String(255), nullable=False)
  file_id = db.Column(db.Integer, db.ForeignKey("files.uuid"), nullable=False)
  polarity = db.Column(db.String(255), nullable=False)
  type = db.Column(db.String(255), nullable=False)
  created = db.Column(db.DateTime(timezone=True), default=db.func.current_timestamp(), nullable=False)
  modified = db.Column(db.DateTime(timezone=True), default=db.func.current_timestamp(), onupdate=db.func.current_timestamp(), nullable=False)

  def __init__(self,file_id,sentence,polarity,type):
    self.file_id = file_id
    self.sentence = sentence
    self.polarity = polarity
    self.type = type

