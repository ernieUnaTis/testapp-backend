from app import db
from sqlalchemy_utils import ChoiceType
from app.models.evaluation import Evaluation
from uuid import uuid4
import datetime, enum

# method to generate uuid
def generate_uuid():
    return str(uuid4())

"""
=============
model File
=============
"""

class File(db.Model):
  __tablename__ = "files"
  uuid = db.Column(db.String(255), nullable=False, unique=True, default=generate_uuid, primary_key=True)
  name = db.Column(db.String(255), nullable=False)
  user_id = db.Column(db.Integer, db.ForeignKey("users.uuid"), nullable=False)
  created = db.Column(db.DateTime(timezone=True), default=db.func.current_timestamp(), nullable=False)
  modified = db.Column(db.DateTime(timezone=True), default=db.func.current_timestamp(), onupdate=db.func.current_timestamp(), nullable=False)
  evaluations = db.relationship(Evaluation, backref='evaluations', lazy=True)
  def __init__(self, name, user_id):
    self.name = name
    self.user_id = user_id