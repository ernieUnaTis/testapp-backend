from app import db
from sqlalchemy_utils import ChoiceType
from app.models.file import File
from uuid import uuid4
import datetime, enum

# method to generate uuid
def generate_uuid():
    return str(uuid4())
"""
=============
model classes
=============
"""

class User(db.Model):
  __tablename__ = "users"
  uuid = db.Column(db.String(255), nullable=False, unique=True, default=generate_uuid, primary_key=True)
  email = db.Column(db.String(255), nullable=False)
  password = db.Column(db.String(255), nullable=False)
  created = db.Column(db.DateTime(timezone=True), default=db.func.current_timestamp(), nullable=False)
  modified = db.Column(db.DateTime(timezone=True), default=db.func.current_timestamp(), onupdate=db.func.current_timestamp(), nullable=False)
  files = db.relationship(File, backref='files', lazy=True)

  def __init__(self, email, password):
    self.email = email
    self.password = password