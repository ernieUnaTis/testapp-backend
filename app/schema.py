
from app import app

from app.models.user import User
from app.models.file import File
from app.models.evaluation import Evaluation
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
ma = Marshmallow(app)

"""
=============
schema classes
=============
"""

class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True
        fields = ('uuid', 'email') # fields to expose

user_schema = UserSchema()
users_schema = UserSchema(many=True)

class FileSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = File
        load_instance = True
        fields = ('uuid', 'name','user_id') # fields to expose

file_schema = FileSchema()
files_schema = FileSchema(many=True)

class EvaluationSchema(ma.SQLAlchemyAutoSchema):
    file_details = fields.Nested(FileSchema)
    class Meta:
        model = Evaluation
        load_instance = True
        fields = ('uuid', 'sentence','file_id','type','polarity')
    file = ma.Nested(FileSchema, many=True)

evaluation_schema = EvaluationSchema()
evaluations_schema = EvaluationSchema(many=True)