from flask import Flask,request, jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from flask_cors import CORS
UPLOAD_FOLDER = 'files'

# create the application instance
app = Flask(__name__)
CORS(app)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config.from_object(Config)

@app.errorhandler(404)
def not_found(error_None):
    app.logger.info('Request URL: %s', request.url)
    app.logger.info('Request BODY: %s', request.get_data())
    response = jsonify( {
        'message':'Recurso no encontrado ' + request.url,
        'status': 404
    })
    response.status_code = 404
    app.logger.info('Response Http Status: %s', response.status_code)

    return response

@app.route("/")
def main():
    app.logger.info("main route")
    print("Creating database tables...")
    db.create_all()
    print("Done!")
    return "Hello World!"

# create the application database instance
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app.models import user,file,evaluation
from app.resources import user,session,files,evaluation