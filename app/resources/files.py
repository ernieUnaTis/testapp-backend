from app import app, db
import os
import urllib.request
import requests
import json
from werkzeug.utils import secure_filename

from app.models.file import File
from app.models.evaluation import Evaluation
from app.schema import file_schema,evaluation_schema


ALLOWED_EXTENSIONS = set(['txt'])

from flask import request, jsonify, Response

def call_external_api(text,file_id):
	payload = { "text": text }
	headers = {"Accept": "application/json", "Content-Type": "application/json" }
	api = "https://sentim-api.herokuapp.com/api/v1/"
	response = requests.post(api,headers=headers, data=json.dumps(payload))
	evaluations = json.loads(response.text)
	for sentence in evaluations["sentences"]:
		new_evaluation = Evaluation(file_id,
		                            sentence["sentence"],
																sentence["sentiment"]["polarity"],
																sentence["sentiment"]["type"])
		db.session.add(new_evaluation)
		db.session.commit()

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

"""
===========================
endpoints for files
===========================
"""

@app.route('/files', methods=['POST'])
def upload_files():
	# Revisar si el request contiene archivos
	if 'files[]' not in request.files:
		resp = jsonify({'message' : 'No file part in the request'})
		resp.status_code = 404
		return resp

	files = request.files.getlist('files[]')
	errors = {}
	success = False

	for file in files:
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
			archivo = app.config['UPLOAD_FOLDER']+"/"+file.filename
			file1 = open(archivo)
			#Guardar Archivo
			new_file = File(file.filename, "1")
			db.session.add(new_file)
			db.session.commit()
			result_file = file_schema.dump(new_file)
			file_id = result_file["uuid"]
			call_external_api(file1.read(),file_id)
			success=True
		else:
			errors[file.filename] = 'File type is not allowed'

	if success and errors:
		resp = jsonify({'message' : "Some Files successfully uploaded, just txt files"})
		resp.status_code = 409
		return resp
	if success:
		resp = jsonify({'message' : 'Files successfully uploaded'})
		resp.status_code = 201
		return resp
	else:
		resp = jsonify({'message' : 'No file successfully uploaded'})
		resp.status_code = 500
		return resp

