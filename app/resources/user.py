
from app import app, db
import re
from app.models.user import User
from app.schema import user_schema

from flask import request, jsonify, make_response

regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

def check(email):
      if(re.search(regex,email)):
            return True
      else:
            return False

def check_domain(email):
      if "@coppel.com" in email:
            return True
      else:
            return False

"""
===========================
endpoints for User CRUD
===========================
"""

@app.route("/users", methods=["POST"])
def create_user():
  email = request.json['email']
  print(email)
  print(check(email))
  if check(email)==False:
      resp = jsonify({'message' : 'Its not email!'})
      resp.status_code = 400
      return resp
  if check_domain(email)==False:
      resp = jsonify({'message' : 'Its not Coppel email!'})
      resp.status_code = 400
      return resp
  password = request.json['password']

  new_user = User(email, password)
  db.session.add(new_user)
  db.session.commit()

  result = user_schema.dump(new_user)

  data = {
        'message': 'New User Created!',
        'status': 201,
        'data': result
  }
  return make_response(jsonify(data))