
from celery import uuid
from app import app
from app.models.evaluation import Evaluation
from app.models.user import User
from app.schema import evaluations_schema,evaluation_schema
import json
from flask import request, jsonify, make_response

"""
===========================
endpoints for Evaluation
===========================
"""

@app.route("/evaluations", methods=["GET"])
def get_all_evaluations():
  evaluations = Evaluation.query.all()
  result = evaluations_schema.dump(evaluations)
  response = make_response(
      jsonify(
          {"data": result}
      ),
      200,
  )
  response.headers["Content-Type"] = "application/json"
  return response

@app.route("/user/<path:uuid>/evaluations", methods=["GET"])
def get_all_evaluations_by_user(uuid):
  evaluations = []
  user = User.query.filter_by(uuid=uuid).first()
  for file in user.files:
    for evaluation in file.evaluations:
      print(evaluation_schema.dump(evaluation))
      evaluations.append(evaluation_schema.dump(evaluation))
  result = evaluations
  response = make_response(
      jsonify(
          {"data": result}
      ),
      200,
  )
  response.headers["Content-Type"] = "application/json"
  return response