from app import app, db

from app.models.user import User
from app.schema import user_schema

from flask import request, jsonify, make_response

"""
===========================
endpoints for Session
===========================
"""

@app.route("/login", methods=["POST"])
def login():
  email = request.json['email']
  password = request.json['password']
  user = User.query.filter_by(email=email, password=password).first()
  if(user!= None):
    result = user_schema.dump(user)
    data = {
        'message': 'Login succesful!',
        'status': 200,
        'data': result
    }
    resp = jsonify({'message' : 'Login succesful!','user':user.uuid,'token':'23456'})
    resp.status_code = 200
  else:
    resp = jsonify({'message' : 'Login failed!'})
    resp.status_code = 404
  return resp

@app.route("/logout", methods=["DELETE"])
def logout():
    data = {
        'message': 'Logout succesful!',
        'status': 204,
    }
    return make_response(jsonify(data))