
from celery import Celery
from celery.schedules import crontab
from app.models.file import File
from datetime import datetime
from os import remove
app = Celery('tasks', broker='redis://redis:6379/0')
import os

@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('hello') every 10 seconds.
    sender.add_periodic_task(10.0, test.s('hello'), name='add every 10')

    # Calls test('world') every 30 seconds
    sender.add_periodic_task(300.0, delete_file.s())

@app.task
def test(arg):
    print(arg)

@app.task
def delete_file():
    now = datetime.now()
    files = File.query.all()
    for file in files:
        diff = now - file.created
        diff_in_hours = diff.total_seconds() / 3600
        print(diff_in_hours)
        if(diff_in_hours>12):
            remove(os.path.join("files/")+file.name)
            print("delete " + file.name)