from os import environ, path

BASE_DIR = path.abspath(path.dirname(__file__))

class Config(object):

  DEBUG = environ.get('DEBUG')
  SQLALCHEMY_DATABASE_URI = 'sqlite:///sentimientos.sqlite'
  SQLALCHEMY_TRACK_MODIFICATIONS = False